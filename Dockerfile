FROM mithoopolly/gui_desktop_base_apps:bionic

RUN apt-get update && \
    apt-get install -y g++ \
    libsqlite3-dev libboost-all-dev lcov libnetcdf-dev \
    qt5-qmake qtbase5-dev qtscript5-dev qtdeclarative5-dev libqt5multimedia5 \
    libqt5multimedia5-plugins qtmultimedia5-dev libqt5webkit5-dev git qt5-default

# Install prerequisites
RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        cabextract \
        gosu \
        gpg-agent \
        p7zip \
        pulseaudio-utils \
        software-properties-common \
        tzdata \
        unzip \
        wget \
        winbind \
        zenity

ENV WINEARCH win64
ENV WINEDEBUG -all,err+all
ENV WINEPATH "C:\pwiz"

# # To be singularity friendly, avoid installing anything to /root
RUN mkdir -p /abc/wineprefix64
ENV WINEPREFIX /abc/wineprefix64

RUN apt-get update \
	&& apt-get install -y --no-install-recommends --no-install-suggests \
		software-properties-common \
		apt-transport-https \
		xvfb
RUN dpkg --add-architecture i386
RUN wget -nc https://dl.winehq.org/wine-builds/winehq.key \
	&& apt-key add winehq.key \
	&& apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/ \
    && add-apt-repository ppa:cybermax-dexter/sdl2-backport \
    && apt-get update \
	&& apt-get install -y --install-recommends \
		winehq-devel \
	&& apt-get clean autoclean \
	&& apt-get autoremove -y \
	&& rm -rf /var/lib/{apt,dpkg,cache,log}/
RUN wget 'https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks'
RUN	mv winetricks /usr/bin/winetricks
RUN chmod +x /usr/bin/winetricks
RUN WINEARCH=win64 WINEPREFIX=/abc/wineprefix64 WINEDLLOVERRIDES="mscoree,mshtml=" wineboot --init && \
	WINEARCH=win64 WINEPREFIX=/abc/wineprefix64 DISPLAY=:0 xvfb-run -a winetricks -q vcrun2013 vcrun2017
RUN WINEARCH=win64 WINEPREFIX=/abc/wineprefix64 wineboot --init && \
	WINEARCH=win64 WINEPREFIX=/abc/wineprefix64 winetricks -q dotnet472 corefonts
RUN WINEARCH=win64 WINEPREFIX=/abc/wineprefix64 wineboot --init && \
	WINEARCH=win64 WINEPREFIX=/abc/wineprefix64 winetricks -q dxvk

COPY msconvert/pwiz-bin-windows-x86_64-vc141-release-3_0_20091_ebd13c1fb.tar.bz2 /tmp/pwiz-bin-windows-x86_64.tar.bz2
RUN mkdir -p /abc/wineprefix64/drive_c/pwiz && tar -f /tmp/pwiz-bin-windows-x86_64.tar.bz2 --directory=/abc/wineprefix64/drive_c/pwiz -xj

RUN chown -R abc:abc /abc/wineprefix64/

COPY msconvert/msconvert.desktop /usr/share/applications/
COPY msconvert/msconvert.dockitem /config/.config/plank/dock1/launchers/
COPY msconvert/msconvert.png /usr/share/icons/
COPY msconvert/msconvert /usr/bin/
COPY msconvert/msConvertGUI /usr/bin/
RUN chmod +x /usr/bin/msconvert
RUN chmod +x /usr/bin/msConvertGUI

RUN mkdir /home/app
RUN cd /home/app && git clone --recursive https://github.com/ElucidataInc/ElMaven.git 
RUN apt-get update && \
    apt-get install -y uuid-dev

ARG TAG=v0.12.0
ARG BRANCH=master
RUN cd /home/app/ElMaven ; git checkout tags/${TAG}; git pull origin ${BRANCH} && qmake NOTESTS=yes CONFIG+=release build.pro && make -j8
# COPY all the elmaven assets to make it show in UI
COPY el-maven/el-maven.desktop /usr/share/applications/
COPY el-maven/el-maven.dockitem /config/.config/plank/dock1/launchers/
COPY el-maven/el-maven.png /usr/share/icons/
COPY el-maven/elmaven /usr/bin/
RUN chmod +x /usr/bin/elmaven